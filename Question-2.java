import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class Main {
    public ArrayList<Integer> grid;

    Main() {
        this.grid = this.randChoices(10);
    }

    private int getNum(ArrayList<Integer> v) {
        int n = v.size();
        int index = (int) (Math.random() * n);
        int num = v.get(index);
        v.set(index, v.get(n - 1));
        v.remove(n - 1);
        return num;
    }

    private ArrayList<Integer> randChoices(int n) {
        ArrayList<Integer> a = new ArrayList<>(n);
        ArrayList<Integer> b = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            a.add(i);
        while (a.size() > 0) {
            b.add(getNum(a));
        }
        return b;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        Main m = new Main();

        JButton button00 = new JButton(m.grid.get(0).toString());
        JButton button01 = new JButton(m.grid.get(1).toString());
        JButton button02 = new JButton(m.grid.get(2).toString());
        JButton button10 = new JButton(m.grid.get(3).toString());
        JButton button11 = new JButton(m.grid.get(4).toString());
        JButton button12 = new JButton(m.grid.get(5).toString());
        JButton button20 = new JButton(m.grid.get(6).toString());
        JButton button21 = new JButton(m.grid.get(7).toString());
        JButton button22 = new JButton(m.grid.get(8).toString());
        JButton button30 = new JButton("#");
        JButton button31 = new JButton(m.grid.get(9).toString());
        JButton button32 = new JButton("*");

        frame.add(button00);
        frame.add(button01);
        frame.add(button02);
        frame.add(button10);
        frame.add(button11);
        frame.add(button12);
        frame.add(button20);
        frame.add(button21);
        frame.add(button22);
        frame.add(button30);
        frame.add(button31);
        frame.add(button32);

        frame.setLayout(new GridLayout(4, 3));
        frame.setSize(400, 400);
        frame.setVisible(true);
        frame.setLayout(null);
    }
}