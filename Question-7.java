public class List {
    private Node head;
    private int size;

    List() {
        this.head = new Node(-1);
        this.size = 0;
    }

    List(int[] values) {
        this();
        for (int i : values) {
            this.addLast(i);
        }
    }

    public void addFisrt(int number) {
        addAt(0, number);
    }

    public void addAt(int index, int number) {
        Node temp = this.head;
        while (index != 0) {
            temp = temp.next;
            index--;
        }
        Node newest = new Node(number);
        newest.next = temp.next;
        if (newest.next != null) {
            newest.next.prev = newest;
        }
        newest.prev = temp;
        temp.next = newest;
        this.size++;
    }

    public void addLast(int number) {
        addAt(size, number);
    }

    public boolean isEmpty() {
        if (this.head.next == null) {
            return true;
        }
        return false;
    }

    public void traverse() {
        Node temp = head.next;
        System.out.print("List : ");
        while (temp != null) {
            System.out.print(temp.data);
            System.out.print(" ");
            temp = temp.next;
        }
        System.out.println("");
    }

    public int removeFirst() {
        return removeAt(0);
    }

    public int removeAt(int index) {
        Node prev = this.head;
        while (index != 0) {
            prev = prev.next;
            index--;
        }
        Node delete = prev.next;
        prev.next = delete.next;
        if (delete.next != null) {
            delete.next.prev = prev;
        }
        this.size--;
        return delete.data;
    }

    public int removeLast() {
        return removeAt(this.size - 1);
    }

    public void remove(int data) {
        Node cur = head.next;
        int index = 0;
        while (cur != null) {
            if (cur.data == data) {
                break;
            }
            cur = cur.next;
            index++;
        }
        if (index != this.size) {
            removeAt(index);
        }
    }

    public int size() {
        return this.size;
    }

    public static void main(String[] args) {
        List l = new List();
        l.addFisrt(5);
        l.addLast(10);
        l.traverse();
        l.removeFirst();
        l.traverse();
    }

}

class Node {
    public Node prev;
    public int data;
    public Node next;

    Node() {
        this(0);
    }

    Node(int number) {
        this.prev = null;
        this.data = number;
        this.next = null;
    }
}