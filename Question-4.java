import java.awt.*;
import java.awt.event.*;

public class Main implements ActionListener, WindowListener {
    String num1 = "0";
    String op = "+";
    String num2 = "0";
    Button a = new Button("1");
    Button b = new Button("2");
    Button c = new Button("3");
    Button d = new Button("4");
    Button e = new Button("5");
    Button g = new Button("6");
    Button h = new Button("7");
    Button i = new Button("8");
    Button j = new Button("9");
    Button k = new Button("0");

    Button ad = new Button("+");
    Button su = new Button("-");
    Button mu = new Button("*");
    Button di = new Button("/");

    Button eq = new Button("=");

    TextField num = new TextField(" ");
    Frame f = new Frame();

    Main() {
        f.add(h);
        f.add(i);
        f.add(j);
        f.add(ad);
        f.add(d);
        f.add(e);
        f.add(g);
        f.add(su);
        f.add(a);
        f.add(b);
        f.add(c);
        f.add(mu);
        f.add(k);
        f.add(eq);
        f.add(di);
        f.add(num);
        a.addActionListener(this);
        b.addActionListener(this);
        c.addActionListener(this);
        d.addActionListener(this);
        e.addActionListener(this);
        g.addActionListener(this);
        h.addActionListener(this);
        i.addActionListener(this);
        j.addActionListener(this);
        k.addActionListener(this);
        ad.addActionListener(this);
        su.addActionListener(this);
        mu.addActionListener(this);
        di.addActionListener(this);
        eq.addActionListener(this);

        f.setLayout(new GridLayout(4, 4));
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public String eval(String a, String o, String b) {
        int x = Integer.parseInt(a);
        int y = Integer.parseInt(b);
        switch (o) {
            case "+":
                return Integer.toString(x + y);
            case "-":
                return Integer.toString(x - y);
            case "*":
                return Integer.toString(x * y);
            case "/":
                int z;
                try {
                    z = x / y;
                } catch (Exception e) {
                    return "";
                }
                return Integer.toString(z);
            default:
                return "";
        }
    }

    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();
        switch (s) {
            case "+":
            case "-":
            case "*":
            case "/":
                if (num2 != "0") {
                    num1 = eval(num1, op, num2);
                    op = s;
                    num2 = "0";
                } else {
                    op = s;
                }
                num.setText("");
                break;
            case "=":
                num.setText(eval(num1, op, num2));
                num1 = "0";
                op = "+";
                num2 = "0";
                break;
            default:
                num2 += s;
                num.setText(num.getText() + s);
        }
    }

    public static void main(String[] args) {
        Main a = new Main();
    }
}