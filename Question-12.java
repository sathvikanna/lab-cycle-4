class UserException extends Exception {
    UserException(String s) {
        super(s);
    }
}

public class Main {
    public static void main(String[] args) {
        int age = 20;
        try {
            if (age < 18) {
                throw new UserException("Illegal Access");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            System.out.println("Done");
        }
    }
}