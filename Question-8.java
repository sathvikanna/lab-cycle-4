import java.awt.*;
import java.awt.event.*;
import java.applet.*;

public class Main extends Applet implements ActionListener {
    String msg = "";
    Button red, orange, green;

    public void init() {
        red = new Button("Red");
        orange = new Button("Orange");
        green = new Button("Green");
        add(red);
        add(orange);
        add(green);
        red.addActionListener(this);
        orange.addActionListener(this);
        green.addActionListener(this);
    }

    public void actionPerformed(ActionEvent ae) {
        String str = ae.getActionCommand();
        if (str.equals("Red")) {
            msg = "Stop";
        } else if (str.equals("Orange")) {
            msg = "Ready";
        } else {
            msg = "Go";
        }
        repaint();
    }

    public void paint(Graphics g) {
        g.drawString(msg, 6, 100);
    }
}