import java.util.Scanner;

class MinandMaxheap
{
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int n;
        System.out.println("enter the no. of integers");
        n=input.nextInt();
        int[] arr=new int[n];
        for(int i=0;i<n;i++)
            arr[i]=input.nextInt();
        minHeap min=new minHeap();
        arr=min.Heapify(arr,n);
        min.display(arr);
        maxHeap max=new maxHeap();
        arr=max.Heapify(arr,n);
        max.display(arr);

    }
}

abstract class Heap{
    public abstract int[] Heapify(int arr[],int capacity);
    public abstract int parent(int index);
    public abstract boolean children(int index);
    public abstract int leftChild(int index);
    public abstract int rightChild(int index);
    public void display(int[] arr)
    {
        for(int i=0;i<arr.length;i++)
            System.out.print(arr[i]+" ");
        System.out.println();
    }
}

class minHeap extends Heap {
    Root r;
    public int [] Heapify(int arr[],int capacity)
    {
        r=new Root(capacity);
        r.capacity=capacity;
        for(int i=0;i<r.capacity;i++)
        {
            r.array[i]=Integer.MAX_VALUE;
            decrease(r.array,i,arr[i]);
        }
        return r.array;
    }

    public void decrease(int[] arr,int index,int value)
    {
        if(index >0) {
            if (value >= arr[parent(index)])
                arr[index] = value;
            else if (value < arr[parent(index)]) {
                arr[index] = arr[parent(index)];
                decrease(arr, parent(index), value);
            }
        }
        else arr[index]=value;
    }

    public int parent(int index)
    {
        if(index>2) {
            if (index % 2 == 0)
                return index / 2;
            else return (index - 1) / 2;
        }
        else return 0;
    }

    public boolean children(int index)
    {
        int l=leftChild(index);
        int r=rightChild(index);
        if(l<this.r.capacity)
            return true;
        if(r<this.r.capacity)
            return true;
        else
            return false;
    }

    public int leftChild(int index)
    {
        return ((2*index)+1);
    }

    public int rightChild(int index)
    {
        return 2*(index+1);
    }

}

class maxHeap extends Heap{
    Root r;
    @Override
    public int[] Heapify(int[] arr, int capacity) {
        r=new Root(capacity);
        r.capacity=capacity;
        for(int i=0;i<r.capacity;i++)
        {
            r.array[i]=Integer.MIN_VALUE;
            increase(r.array,i,arr[i]);
        }
        return r.array;
    }

    public void increase(int[] arr,int index,int value)
    {
        if(index >0) {
            if (value <= arr[parent(index)])
                arr[index] = value;
            else if (value > arr[parent(index)]) {
                arr[index] = arr[parent(index)];
                increase(arr, parent(index), value);
            }
        }
        else arr[index]=value;
    }

    @Override
    public int parent(int index) {
        if(index>2) {
            if (index % 2 == 0)
                return index / 2;
            else return (index - 1) / 2;
        }
        else return 0;
    }

    @Override
    public boolean children(int index) {
        int l=leftChild(index);
        int r=rightChild(index);
        if(l<this.r.capacity)
            return true;
        if(r<this.r.capacity)
            return true;
        else
            return false;
    }

    @Override
    public int leftChild(int index) {
        return ((2*index)+1);
    }

    @Override
    public int rightChild(int index) {
        return 2*(index+1);
    }

}

class Root
{
    int capacity;
    int[] array;
    Root()
    {
        this.capacity=10;
        this.array=new int[10];
    }
    Root(int capacity)
    {
        this.capacity=capacity;
        this.array=new int[this.capacity];
    }
    public boolean isEmpty(int[] arr)
    {
        if(arr.length==0)
            return true;
        else
            return false;
    }
    public boolean isFull(int[] arr)
    {
        if(arr.length==this.capacity)
            return true;
        else return false;
    }
}