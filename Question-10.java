public class List {
    private Node head;
    private int size;

    List() {
        head = new Node(-1);
        size = 0;
    }

    List(int[] values) {
        this();
        for (int i : values) {
            this.addLast(i);
        }
    }

    public void addFisrt(int number) {
        addAt(0, number);
    }

    public void addAt(int index, int number) {
        Node temp = this.head;
        while (index != 0) {
            temp = temp.next;
            index--;
        }
        Node newest = new Node(number);
        newest.next = temp.next;
        temp.next = newest;
        this.size++;
    }

    public boolean isEmpty() {
        if (this.head.next == null) {
            return true;
        }
        return false;
    }

    public void addLast(int number) {
        addAt(size, number);
    }

    public void traverse() {
        Node temp = head.next;
        System.out.print("List : ");
        while (temp != null) {
            System.out.print(temp.data);
            System.out.print(" ");
            temp = temp.next;
        }
        System.out.println("");
    }

    public int removeFirst() {
        return removeAt(0);
    }

    public int removeAt(int index) {
        Node prev = this.head;
        while (index != 0) {
            prev = prev.next;
            index--;
        }
        Node delete = prev.next;
        prev.next = delete.next;
        this.size--;
        return delete.data;
    }

    public int removeLast() {
        return removeAt(this.size - 1);
    }

    public void remove(int data) {
        Node cur = head.next;
        int index = 0;
        while (cur != null) {
            if (cur.data == data) {
                break;
            }
            cur = cur.next;
            index++;
        }
        if (index != this.size) {
            removeAt(index);
        }
    }

    public int size() {
        return this.size;
    }

    public static void main(String[] args) {
        int[] values1 = { 5, 7, 17, 13, 11 };
        int[] values2 = { 12, 10, 12, 4, 6 };
        List list1 = new List(values1);
        List list2 = new List(values2);
        for (int i = 1; i <= list1.size(); i += 2) {
            list1.addAt(i, list2.removeFirst());
        }
        list1.traverse();
    }
}

class Node {
    public int data;
    public Node next;

    Node() {
        this(0);
    }

    Node(int number) {
        this.data = number;
        this.next = null;
    }
}