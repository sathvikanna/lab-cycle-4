public abstract class Shape {
    public double firstDimension;
    public double secondDimension;

    Shape(double a, double b) {
        this.firstDimension = a;
        this.secondDimension = b;
    }

    public abstract double Area();
}

public class Circle extends Shape {
    Circle(double radius) {
        super(radius, radius);
    }

    public double Area() {
        return (3.14 * this.firstDimension * this.secondDimension);
    }
}

public class Triangle extends Shape {
    Triangle(int base, int height) {
        super(base, height);
    }

    public double Area() {
        return (0.5 * this.firstDimension * this.secondDimension);
    }

}

public class Rectangle extends Shape {
    Rectangle(int a, int b) {
        super(a, b);
    }

    public double Area() {
        return (this.firstDimension * this.secondDimension);
    }

}