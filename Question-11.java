

import java.util.Scanner;

public class CircularDLList {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int n,k;
        System.out.println("enter the no. of elements");
        n=input.nextInt();
        circularDLL c1=new circularDLL();
        for(int i=0;i<n;i++)
        {
            System.out.print("enter the value: ");
            k=input.nextInt();
            c1.insertNFirst(k);
        }
        System.out.print("the present list: ");
        c1.display();
        c1.insertNFirst(100);
        System.out.print("inserting 100 at first: ");
        c1.display();
        c1.insertNEnd(1000);
        System.out.print("inserting 1000 at end: ");
        c1.display();
        c1.insertNIndex(3,0);
        System.out.print("inserting 0 at index 3: ");
        c1.display();
        c1.deleteNFirst();
        System.out.print("deleting at first: ");
        c1.display();
        c1.deleteNEnd();
        System.out.print("deleting at end: ");
        c1.display();
        c1.deleteNIndex(2);
        System.out.print("deleting element at index 2: ");
        c1.display();
    }
}

class circularDLL{
    int capacity=0;
    CDNode root;
    circularDLL(){
        this.root=null;
    }
    circularDLL(int value)
    {
        insertNFirst(value);
    }

    public boolean isEmpty(CDNode root)
    {
        if(root==null)
            return true;
        else return false;
    }

    public void display()
    {
        CDNode temp=this.root;
        for(int i=0;i<this.capacity;i++)
        {
            System.out.print(temp.value+" ");
            temp=temp.right;
        }
        System.out.println();
    }


    public void insertNFirst(int value)
    {
        if(isEmpty(this.root)) {
            System.out.println("1st value insertion");
            this.root=new CDNode(value);
            if(!isEmpty(this.root)) {
                this.capacity++;
                this.root.left=this.root;
                this.root.right = this.root;
            }
            else
                System.out.println("the value not inserted may be space not available");
        }
        else{
            CDNode newnode=new CDNode(value);
            if(!isEmpty(newnode)) {
                this.capacity++;
                newnode.right = this.root;
                newnode.left = this.root.left;
                this.root.left.right = newnode;
                this.root.left=newnode;
                this.root = newnode;
            }
            else
                System.out.println("the value is not inserted may be space not available");
        }
    }

    public void insertNIndex(int index, int value)
    {
        if(index ==0) insertNFirst(value);
        else if(index == this.capacity) insertNEnd(value);
        else if(index>0 && index<this.capacity)
        {
            CDNode temp=this.root;
            for(int i=0;i<index-1;i++)
            {
               temp=temp.right;
            }
            CDNode newnode=new CDNode(value);
            if(!isEmpty(newnode)) {
                this.capacity++;
                newnode.left = temp;
                newnode.right = temp.right;
                newnode.left.right = newnode;
                newnode.right.left = newnode;
            }
            else System.out.println("the value is not inserted may be space not available");
        }
        else System.out.println("index is out of range");
    }

    public void insertNEnd(int value)
    {
        if(isEmpty(this.root)) insertNFirst(value);
        else
        {
            CDNode newnode=new CDNode(value);
            if(!isEmpty(newnode))
            {
                this.capacity++;
                newnode.left=this.root.left;
                newnode.right=this.root;
                newnode.left.right=newnode;
                root.left=newnode;
            }
            else System.out.println("the value is not inserted amy be space not available");
        }
    }

    public void deleteNFirst()
    {
        if(isEmpty(this.root))
            System.out.println("the list is empty");
        else if(this.capacity==1){
            this.root=null;
            this.capacity--;
        }
        else{
            CDNode temp=this.root;
            this.root=temp.right;
            this.root.left=temp.left;
            temp.left.right=this.root;
            temp.left=null;
            temp.right=null;
            this.capacity--;
        }
    }

    public void deleteNEnd()
    {
        if(isEmpty(this.root))
            System.out.println("the list is empty");
        else if(this.capacity==1){
            this.root=null;
            this.capacity--;
        }
        else{
            CDNode temp=this.root.left;
            this.root.left=temp.left;
            temp.left.right=this.root;
            temp.left=null;
            temp.right=null;
            this.capacity--;
        }
    }

    public void deleteNIndex(int index)
    {
        if(index==0) deleteNFirst();
        else if(index==this.capacity-1) deleteNEnd();
        else if(index>0 && index<this.capacity)
        {
            CDNode temp=this.root;
            for(int i=0;i<index;i++)
            {
                temp=temp.right;
            }
            temp.left.right=temp.right;
            temp.right.left=temp.left;
            temp.left=null;
            temp.right=null;
            this.capacity--;
        }
        else System.out.println("the given index is out of range");
    }
}

class CDNode{
    int value;
    CDNode left=null;
    CDNode right=null;
    CDNode()
    {
        System.out.println("No value passed");
    }
    CDNode(int value)
    {
        this.value=value;
    }
}